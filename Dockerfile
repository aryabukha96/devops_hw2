FROM python:alpine
WORKDIR /godot

ENV DEBIAN_FRONTEND noninteractive
RUN apt update
RUN apt -y install clang build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev \
    libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm
RUN sed -i "lc\#! /usr/bin/python3" /usr/bin/scons
EXPOSE 5000

COPY . .
CMD tail -f /dev/null